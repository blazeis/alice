from libs.Enums import dim_range, CapTypes, DevTypes, DevStates
import requests


class Device:

    def __init__(self, config, logger):
        self.json_query = ""
        self.log = logger
        self.config = config
        self.info = {
            'id': "",
            'name': "",
            'description': "",
            'room': "",
            'type': DevTypes.LIGHT.value
        }
        self.capabilities = self.getCapabilities()
        self.state = DevStates.OFF
        self.manufacturer_info = {
            'manufacturer': "",
            'model': ""
        }

    def setManufacturer(self, manufacturer: str):
        self.manufacturer_info['manufacturer'] = manufacturer

    def getManufacturer(self):
        return self.manufacturer_info['manufacturer']

    def setModel(self, model: str):
        self.manufacturer_info['model'] = model

    def getManufacturerInfo(self):
        return self.manufacturer_info['model']

    def setId(self, dev_id: str):
        self.info['id'] = dev_id

    def getId(self):
        return self.info['id']

    def setName(self, name: str):
        self.info['name'] = name

    def getName(self):
        return self.info['name']

    def setDescription(self, description: str):
        self.info['description'] = description

    def getDescription(self):
        return self.info['description']

    def setRoom(self, room: str):
        self.info['room'] = room

    def getRoom(self):
        return self.info['room']

    def setDevType(self, dev_type: DevTypes):
        self.info['type'] = dev_type.value

    def getDevType(self):
        return self.info['type']

    def getCapabilities(self):
        cap_on_off = {
            'type': CapTypes.ON_OFF.value
        }
        return [cap_on_off]

    def getDev(self):
        res_dict = self.info.copy()
        res_dict.update({'capabilities': self.capabilities})
        res_dict.update({'device_info': self.manufacturer_info})
        return res_dict

    def getDevState(self):
        res = {
            'id': self.getId(),
            'capabilities': [self.getOnOffState()]
        }
        return res

    def getOnOffState(self):
        res = {
            'type': CapTypes.ON_OFF.value,
            'state': {
                'instance': 'on',
                'value': True if self.getState() == DevStates.ON else False
            }
        }
        return res

    def setState(self, state):
        if isinstance(state, str):
            if state in ['on', 'true']:
                self.state = DevStates.ON
            elif state in ['off', 'false']:
                self.state = DevStates.OFF
        elif isinstance(state, bool):
            if state is True:
                self.state = DevStates.ON
            elif state is False:
                self.state = DevStates.OFF
        else:
            self.state = state
        if self.state == DevStates.ON:
            r = requests.get("{}/set/{}.state?value=1".format(self.config.getApiUrl(), self.getId()))
            code = r.status_code
        else:
            r = requests.get("{}/set/{}.state?value=0".format(self.config.getApiUrl(), self.getId()))
            code = r.status_code
        if code != 200:
            self.log.debug("ERROR SETTING STATE: {}, DEV: {}, CODE: {}".format(state, self.getId(), code))
            return False
        else:
            self.log.debug("SETTED STATE: {}, DEV: {}".format(state, self.getId()))
            return True

    def getState(self):
        state_req = requests.get("{}/getPlainValue/{}.state".format(self.config.getApiUrl(), self.getId()))
        state = state_req.content.decode('utf-8')
        state = 0
        self.log.debug("### Readed: {}".format(state))
        if state == 'true':
            state = 1
        elif state == 'false':
            state = 0
        try:
            if int(state) == 1:
                self.state = DevStates.ON
            else:
                self.state = DevStates.OFF
        except Exception as e:
            self.log.debug("exception: {}".format(e))
        return self.state

    def setBrightness(self, brightness: int = 0):
        return self.setState(DevStates.ON if brightness > 0 else DevStates.OFF)

    def getBrightness(self):
        return dim_range['max'] if self.getState() == DevStates.ON else dim_range['min']
