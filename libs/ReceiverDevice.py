from libs.Enums import dim_range, CapTypes, DevStates
from libs.Device import Device
import requests


class ReceiverDevice(Device):
    def __init__(self, config, logger):
        self.range = dim_range.copy()
        self.range_channel = dim_range.copy()
        self.volume = 7
        self.channel = 6
        self.channel_id = ""
        super().__init__(config=config, logger=logger)

    def setVolume(self, volume: int = 0):
        if self.volume != volume:
            self.volume = volume
            r = requests.get("{}fn=write&alias={}&value={}".format(self.config.getApiUrl(),
                                                                   self.getId(),
                                                                   self.volume))
            code = r.status_code
            if code != 200:
                self.log.debug("ERROR SETTING VOLUME: {}, DEV: {}, CODE: ".format(volume, self.getId(), code))
                return False
            else:
                self.log.debug("setted volume: {}, DEV: {}".format(volume, self.getId()))
                return True
        return True

    def getVolume(self):
        state_req = requests.get("{}fn=getvalue&alias={}".format(self.config.getApiUrl(), self.getId()))
        state = state_req.content
        self.log.debug("### Readed: {}".format(state))
        try:
            self.volume = int(state)
        except Exception as e:
            self.log.debug("exception: {}".format(e))
        return (self.volume-1)*4

    def setChannelId(self, dev_id: str):
        self.channel_id = dev_id

    def getChannelId(self):
        return self.channel_id

    def setChannel(self, channel: int = 0):
        if self.channel != channel:
            #self.channel = channel
            self.channel = 6
            r = requests.get("{}fn=write&alias={}&value={}".format(self.config.getApiUrl(),
                                                                   self.getChannelId(),
                                                                   self.channel))
            code = r.status_code
            if code != 200:
                self.log.debug("ERROR SETTING CHANNEL: {}, DEV: {}, CODE: ".format(channel, self.getChannelId(), code))
                return False
            else:
                self.log.debug("setted channel: {}, DEV: {}".format(channel, self.getChannelId()))
                return True
        return True

    def getChannel(self):
        state_req = requests.get("{}fn=getvalue&alias={}".format(self.config.getApiUrl(), self.getChannelId()))
        state = state_req.content
        self.log.debug("### Readed: {}".format(state))
        try:
            self.channel = int(state)
        except Exception as e:
            self.log.debug("exception: {}".format(e))
        return self.channel

    def getDevState(self):
        res = {
            'id': self.getId(),
            'capabilities': [self.getVolumeState(), self.getChannelState()]
        }
        return res

    def getState(self):
        state_req = requests.get("{}fn=getvalue&alias={}".format(self.config.getApiUrl(), self.getId()))
        state = state_req.content
        self.log.debug("### Readed: {}".format(state))
        try:
            if bool(state) > 1:
                self.state = DevStates.ON
            else:
                self.state = DevStates.OFF
        except Exception as e:
            self.log.debug("exception: {}".format(e))
        return self.state

    def setState(self, state):
        if isinstance(state, str):
            if state in ['on', 'true']:
                self.state = DevStates.ON
            elif state in ['off', 'false']:
                self.state = DevStates.OFF
        elif isinstance(state, bool):
            if state is True:
                self.state = DevStates.ON
            elif state is False:
                self.state = DevStates.OFF
        else:
            self.state = state
        if self.state == DevStates.ON:
            r = requests.get("{}fn=write&alias={}&value=7".format(self.config.getApiUrl(), self.getId()))
            code = r.status_code
        else:
            r = requests.get("{}fn=write&alias={}&value=1".format(self.config.getApiUrl(), self.getId()))
            code = r.status_code
        if code != 200:
            self.log.debug("ERROR SETTING STATE: {}, DEV: {}, CODE: {}".format(state, self.getId(), code))
            return False
        else:
            self.log.debug("SETTED STATE: {}, DEV: {}".format(state, self.getId()))
            return True

    def getVolumeState(self):
        res = {
            'type': CapTypes.RANGE.value,
            'state': {
                'instance': 'volume',
                'value': self.getVolume()
            }
        }
        return res

    def getChannelState(self):
        res = {
            'type': CapTypes.RANGE.value,
            'state': {
                'instance': 'channel',
                'value': self.getChannel()
            }
        }
        return res

    def getCapabilities(self):
        cap_dim = {'type': CapTypes.RANGE.value,
                   'retrievable': True,
                   'parameters': {
                       'instance': 'volume',
                       'range': self.range
                        }
                   }
        cap_ch = {'type': CapTypes.RANGE.value,
                  'retrievable': True,
                  'parameters': {
                      'instance': 'channel',
                      'range': self.range_channel
                    }
                  }
        return [cap_dim, cap_ch]
