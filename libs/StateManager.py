from time import time
import pickle


class StateManager:

    def __init__(self, logger, cache_path):
        self.__log = logger
        self.__cache_path = "{}.cache".format(cache_path)
        self.__keys = {}
        self.__tokens = {}
        self.load()
        self.__log.info("StateManager loaded")

    def addKey(self, user, key):
        self.__keys[str(key)] = (user, time())

    def checkKey(self, key):
        val = self.__keys.get(str(key), None)
        if val is None:
            return None
        if val[1] + 300 < time():
            self.__keys.pop(str(key), None)
            return None
        else:
            return val[0]

    def addToken(self, user, token):
        self.__tokens[str(token)] = (user, time())
        self.save()

    def checkToken(self, token):
        user = self.__tokens.get(str(token), None)
        if user is None:
            return user
        else:
            return user[0]

    def removeToken(self, token):
        self.__tokens.pop(str(token), None)

    def save(self):
        file = open(self.__cache_path, 'wb')
        file.write(pickle.dumps([self.__keys, self.__tokens]))
        file.close()
        self.__log.info("State cache saved")

    def load(self):
        try:
            file = open(self.__cache_path, 'rb')
            cache = pickle.loads(file.read())
            file.close()
            self.__keys = cache[0]
            self.__tokens = cache[1]
            self.__log.info("State cache loaded, tokens length: {}".format(len(self.__tokens)))
        except Exception as load_err:
            self.__log.info("State cache err: {}".format(load_err))
            self.save()
