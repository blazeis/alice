import logging
import sys
import json
import os
from random import randint
from flask import Flask, request, redirect, jsonify, abort, render_template
from libs.ConfigManager import ConfigManager
from libs.DeviceManager import DeviceManager
from libs.Enums import CapTypes, DevTypes
from libs.StateManager import StateManager
import threading
import time

log = logging.getLogger("MAIN")
log.setLevel(logging.INFO)
formatter = logging.Formatter('%(asctime)s %(name)-13s %(threadName)-10s %(levelname)-8s %(message)s')

ch_e = logging.StreamHandler(stream=sys.stderr)
ch_e.setLevel(logging.ERROR)
ch_e.setFormatter(formatter)
log.addHandler(ch_e)

ch = logging.StreamHandler(stream=sys.stdout)
ch.setLevel(logging.DEBUG)
ch.setFormatter(formatter)
log.addHandler(ch)

log.setLevel(logging.DEBUG)

config_log = logging.getLogger("Config")
config_log.name = "CONFIG"
config_log.setLevel(logging.DEBUG)
config_log.addHandler(ch)
config_log.addHandler(ch_e)

device_log = logging.getLogger("Device")
device_log.name = "DEVICE"
device_log.setLevel(logging.DEBUG)
device_log.addHandler(ch)
device_log.addHandler(ch_e)

state_log = logging.getLogger("State")
state_log.name = "STATE"
state_log.setLevel(logging.DEBUG)
state_log.addHandler(ch)
state_log.addHandler(ch_e)

app = Flask(__name__)

path_name = os.path.dirname(sys.argv[0])
full_path = os.path.abspath(path_name)
cache_path = full_path + "/cache/state"

config = ConfigManager(config_log)
device_manager = DeviceManager(config, device_log)
state_manager = StateManager(state_log, cache_path=cache_path)
dev_lock = threading.RLock()

@app.route(config.getBaseUri(), methods=['GET', 'POST'])
def index():
    log.debug("{} requested".format(request.path))
    return 'OK'


@app.route("{}oauth/authorize/".format(config.getBaseUri()), methods=['GET'])
def auth():
    log.debug("{} requested".format(request.path))
    redirect_uri = request.args['redirect_uri']
    state = request.args['state']
    if redirect_uri is not None and state is not None:
        return render_template("auth.html",
                               static_path="{}static".format(config.getBaseUri()),
                               redirect_uri=redirect_uri,
                               state=state,
                               submit_uri="{}oauth/authorize/".format(config.getBaseUri()))
    else:
        return abort(400)


@app.route("{}oauth/authorize/".format(config.getBaseUri()), methods=['POST'])
def check_auth():
    log.debug("{} requested".format(request.path))
    login = request.form['username']
    passwd = request.form['password']
    redirect_uri = request.form['redirect_uri']
    state = request.form['state']
    if redirect_uri is not None and state is not None:
        if login == config.getOauthUser() and passwd == config.getOauthPass():
            key = randint(-sys.maxsize - 1, sys.maxsize)
            state_manager.addKey(login, key)
            log.debug("sending to: {}?state={}&code={}".format(redirect_uri, state, key))
            return redirect("{}?state={}&code={}".format(redirect_uri, state, key))
        else:
            return abort(403)
    else:
        return abort(400)


@app.route("{}oauth/token/".format(config.getBaseUri()), methods=['POST'])
def get_token():
    log.debug("{} requested".format(request.path))
    code = request.form['code']
    user = state_manager.checkKey(code)
    if user is None:
        log.debug("Key invalid: {}".format(code))
        return abort(403)
    token = randint(-sys.maxsize - 1, sys.maxsize)
    state_manager.addToken(user=user, token=token)
    json_form = dict()
    json_form['access_token'] = token
    log.debug("token: {}".format(json_form))
    return jsonify(json_form)


@app.route("{}v1.0/user/devices".format(config.getBaseUri()), methods=['GET'])
def get_device_list():
    log.debug("{} requested".format(request.path))
    token = request.headers['Authorization'].split()[1]
    user = state_manager.checkToken(token)
    if user is None:
        return abort(403)
    res_devices = []
    for dev in device_manager.devices:
        res_devices.append(dev.getDev())
    res_dict = {
        'request_id': request.headers['X-Request-Id'],
        'payload': {
            'user_id': 'yandex',
            'devices': res_devices
        }
    }
    log.debug(json.dumps(res_dict, ensure_ascii=False))
    return json.dumps(res_dict, ensure_ascii=False)


@app.route("{}v1.0/user/unlink".format(config.getBaseUri()), methods=['POST'])
def user_unlink():
    log.debug("{} requested".format(request.path))
    token = request.headers['Authorization'].split()[1]
    user = state_manager.checkToken(token)
    if user is None:
        return abort(403)
    res_dict = {
        'request_id': request.headers['X-Request-Id']
    }
    return json.dumps(res_dict, ensure_ascii=False)


@app.route("{}v1.0/user/devices/query".format(config.getBaseUri()), methods=['POST'])
def device_query():
    log.debug("{} requested".format(request.path))
    log.debug("request: {}".format(request.json))
    current_milli_time = int(round(time.time() * 1000))
    token = request.headers['Authorization'].split()[1]
    user = state_manager.checkToken(token)
    if user is None:
        return abort(403)
    devs = request.json.get('devices', None)
    if devs is None:
        log.debug("device list is empty!")
        return abort(400)
    res_dev = []
    t_list = []
    for dev in device_manager.devices:
        for r_dev in devs:
            if r_dev['id'] == dev.getId():
                log.debug("adding device {}".format(dev.getId()))
                t = threading.Thread(target=addDeviceState, args=(dev, res_dev))
                t.start()
                t_list.append(t)
    for t in t_list:
        t.join()
    res_dict = {
        'request_id': request.headers['X-Request-Id'],
        'payload': {
            'devices': res_dev
        }
    }
    log.debug(json.dumps(res_dict, ensure_ascii=False))
    log.debug("time: {}".format(int(round(time.time() * 1000))-current_milli_time))
    return json.dumps(res_dict, ensure_ascii=False)


@app.route("{}v1.0/user/devices/action".format(config.getBaseUri()), methods=['POST'])
def device_action():
    log.debug("{} requested".format(request.path))
    json_data = request.json
    log.debug("{}".format(json_data))
    token = request.headers['Authorization'].split()[1]
    user = state_manager.checkToken(token)
    if user is None:
        return abort(403)
    payload = json_data.get('payload', None)
    if payload is None:
        log.error("Empty payload in request: {}".format(request))
        return abort(400)
    devices = payload.get('devices', None)
    if devices is None or not isinstance(devices, list):
        log.error("Device format error in request: {}".format(request))
        return abort(400)
    res_dev = []
    for dev in devices:
        dev_id = dev.get('id', None)
        dev_caps = dev.get('capabilities', None)
        if dev_id is None or dev_caps is None or not isinstance(dev_caps, list):
            log.error("Device id is None or dev_caps error in device: {}".format(dev))
            return abort(400)
        res_cap = {
            'id': dev_id,
            'capabilities': []
        }
        for cap in dev_caps:
            cap_type = cap.get("type", None)
            cap_state = cap.get("state", None)
            if cap_type is None or cap_state is None:
                log.error("cap_type or cap_state is none for dev {}".format(dev))
                return abort(400)
            cap_inst = cap_state.get('instance', None)
            cap_value = cap_state.get('value', None)
            cap_relative = cap_state.get('relative', False)
            if cap_inst is None or cap_value is None:
                log.error("cap_inst or cap_value is none for dev {}".format(dev))
                return abort(400)
            result = False
            r_text = "Устройство не найдено"
            for s_dev in device_manager.devices:
                if s_dev.getId() == dev_id:
                    if cap_type == CapTypes.ON_OFF.value:
                        result = s_dev.setState(cap_value)
                        if result is False:
                            r_text = "Ошибка при отправке команды"
                    elif cap_type == CapTypes.RANGE.value:
                        if s_dev.getDevType() == DevTypes.LIGHT.value:
                            result = s_dev.setBrightness(cap_value)
                        elif s_dev.getDevType() == DevTypes.THERMOSTAT.value:
                            if cap_relative is True:
                                temp = s_dev.getTemperature() + cap_value
                            else:
                                temp = cap_value
                            result = s_dev.setTemperature(temp)
                        elif s_dev.getDevType() == DevTypes.MEDIA_DEVICE.value:
                            if cap_inst == "volume":
                                if cap_relative is True:
                                    if cap_value > 0:
                                        if cap_value < s_dev.range['precision']:
                                            cap_value = s_dev.range['precision']
                                    else:
                                        if abs(cap_value) < s_dev.range['precision']:
                                            cap_value = 0 - s_dev.range['precision']
                                    volume = s_dev.getVolume() + cap_value
                                    if volume > s_dev.range['max']:
                                        volume = s_dev.range['max']
                                    elif volume < s_dev.range['min']:
                                        volume = s_dev.range['min']
                                    volume = int(volume / 4) + 1
                                else:
                                    volume = int(cap_value/4) + 1
                                result = s_dev.setVolume(volume)
                            elif cap_inst == "channel":
                                result = s_dev.setChannel(cap_value)
                        else:
                            log.debug("DevType unknown: {}".format(s_dev.getDevType()))
                            result = False
                        if result is False:
                            r_text = "Ошибка при отправке команды"
            res_status = {}
            if result is True:
                res_status.update({'status': 'DONE'})
            else:
                res_status.update({'status': 'ERROR',
                                   'error_code': 'INVALID_ACTION',
                                   'error_message': r_text})
            res_inst = {
                'instance': cap_inst,
                'value': cap_value,
                'action_result': res_status
            }
            res_state = {
                'type': cap_type,
                'state': res_inst
            }
            res_cap['capabilities'].append(res_state)
        res_dev.append(res_cap)
    res_dict = {
        'request_id': request.headers['X-Request-Id'],
        'payload': {'devices': res_dev}
    }
    log.debug(json.dumps(res_dict, ensure_ascii=False))
    return json.dumps(res_dict, ensure_ascii=False)


def addDeviceState(dev, list_devs):
    state = dev.getDevState()
    dev_lock.acquire()
    list_devs.append(state)
    dev_lock.release()
