from enum import Enum


dim_range = {
    'min': 0,
    'max': 100,
    'precision': 10
}


class DevTypes(Enum):
    LIGHT = 'devices.types.light'
    SOCKET = 'devices.types.socket'
    SWITCH = 'devices.types.switch'
    THERMOSTAT = 'devices.types.thermostat'
    CONDITIONER = 'devices.types.thermostat.ac'
    MEDIA_DEVICE = 'devices.types.media_device'
    TV = 'devices.types.media_device.tv'
    COOKING = 'devices.types.cooking'
    KETTLE = 'devices.types.cooking.kettle'
    OTHER = 'devices.types.other'


class CapTypes(Enum):
    ON_OFF = 'devices.capabilities.on_off'
    COLOR = 'devices.capabilities.color_setting'
    MODE = 'devices.capabilities.mode'
    RANGE = 'devices.capabilities.range'
    TOGGLE = 'devices.capabilities.toggle'


class DevStates(Enum):
    ON = 'on'
    OFF = 'off'
