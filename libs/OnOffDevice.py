from libs.Enums import DevStates
from libs.Device import Device
import requests


class OnOffDevice(Device):

    def setState(self, state):
        if isinstance(state, str):
            if state in ['on', 'true']:
                self.state = DevStates.ON
            elif state in ['off', 'false']:
                self.state = DevStates.OFF
        elif isinstance(state, bool):
            if state is True:
                self.state = DevStates.ON
            elif state is False:
                self.state = DevStates.OFF
        else:
            self.state = state
        if self.state == DevStates.ON:
            r = requests.get("{}/set/{}.state?value=1".format(self.config.getApiUrl(), self.getId()))
            code = r.status_code
        else:
            r = requests.get("{}/set/{}.state?value=0".format(self.config.getApiUrl(), self.getId()))
            code = r.status_code
        if code != 200:
            self.log.debug("ERROR SETTING STATE: {}, DEV: {}, CODE: {}".format(state, self.getId(), code))
            return False
        else:
            self.log.debug("SETTED STATE: {}, DEV: {}".format(state, self.getId()))
            return True
