from libs.Enums import dim_range, CapTypes
from libs.Device import Device
import requests


class DimmableDevice(Device):

    def __init__(self, config, logger):
        self.range = dim_range.copy()
        self.brightness = 0
        super().__init__(config=config, logger=logger)

    def setBrightness(self, brightness: int = 0):
        if self.getBrightness() != brightness:
            self.brightness = brightness
            r = requests.get("{}/set/{}.brightness?value={}".format(self.config.getApiUrl(), self.getId(), self.brightness))
            code = r.status_code
            if code != 200:
                self.log.debug("ERROR SETTING BRIGHTNESS: {}, DEV: {}, CODE: ".format(brightness, self.getId(), code))
                return False
            else:
                self.log.debug("setted brightness: {}, DEV: {}".format(brightness, self.getId()))
                return True
        return True

    def getCapabilities(self):
        cap_dim = {'type': CapTypes.RANGE.value,
                   'retrievable': True,
                   'parameters': {
                       'instance': 'brightness',
                       'unit': 'unit.percent',
                       'range': self.range
                        }
                   }
        cap_on_off = {
            'type': CapTypes.ON_OFF.value
        }
        return [cap_dim, cap_on_off]

    def getDevState(self):
        res = {
            'id': self.getId(),
            'capabilities': [self.getOnOffState(), self.getDimmerState()]
        }
        return res

    def getDimmerState(self):
        res = {
            'type': CapTypes.RANGE.value,
            'state': {
                'instance': 'brightness',
                'value': self.getBrightness()
            }
        }
        return res

    def getBrightness(self):
        state_req = requests.get("{}/getPlainValue/{}.brightness".format(self.config.getApiUrl(), self.getId()))
        state = state_req.content.decode('utf-8')
        self.log.debug("### Readed: {}".format(state))
        if state == 'true':
            state = 100
        elif state == 'false':
            state = 0
        try:
            self.brightness = int(state)
        except Exception as e:
            self.log.debug("exception: {}".format(e))
        return self.brightness
