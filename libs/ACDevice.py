from libs.Enums import dim_range, CapTypes, DevStates
from libs.Device import Device
import requests


class ACDevice(Device):
    def __init__(self, config, logger):
        self.range = dim_range.copy()
        self.temp = 22
        self.temp_id = ""
        super().__init__(config=config, logger=logger)

    def setTempId(self, temp_id: str):
        self.temp_id = temp_id

    def getTempId(self):
        return self.temp_id

    def setTemperature(self, temp: int = 0):
        if self.temp != temp:
            self.temp = temp
            r = requests.get("{}fn=write&alias={}&value={}".format(self.config.getApiUrl(),
                                                                   self.getTempId(),
                                                                   self.temp))
            code = r.status_code
            if code != 200:
                self.log.debug("ERROR SETTING TEMPERATURE: {}, DEV: {}, CODE: ".format(temp, self.getId(), code))
                return False
            else:
                self.log.debug("setted temperature: {}, DEV: {}".format(temp, self.getId()))
                return True
        return True

    def getTemperature(self):
        state_req = requests.get("{}fn=getvalue&alias={}".format(self.config.getApiUrl(), self.getTempId()))
        state = state_req.content
        self.log.debug("### Readed: {}".format(state))
        try:
            self.temp = int(state)
        except Exception as e:
            self.log.debug("exception: {}".format(e))
        return self.temp

    def getDevState(self):
        res = {
            'id': self.getId(),
            'capabilities': [self.getOnOffState(), self.getTemperatureState()]
        }
        return res

    def getState(self):
        state_req = requests.get("{}fn=getvalue&alias={}".format(self.config.getApiUrl(), self.getId()))
        state = state_req.content
        self.log.debug("### Readed: {}".format(state))
        try:
            if bool(state) > 10:
                self.state = DevStates.ON
            else:
                self.state = DevStates.OFF
        except Exception as e:
            self.log.debug("exception: {}".format(e))
        return self.state

    def setState(self, state):
        if isinstance(state, str):
            if state in ['on', 'true']:
                self.state = DevStates.ON
            elif state in ['off', 'false']:
                self.state = DevStates.OFF
        elif isinstance(state, bool):
            if state is True:
                self.state = DevStates.ON
            elif state is False:
                self.state = DevStates.OFF
        else:
            self.state = state
        if self.state == DevStates.ON:
            r = requests.get("{}fn=write&alias={}&value=true".format(self.config.getApiUrl(), self.getId()))
            code = r.status_code
        else:
            r = requests.get("{}fn=write&alias={}&value=false".format(self.config.getApiUrl(), self.getId()))
            code = r.status_code
        if code != 200:
            self.log.debug("ERROR SETTING STATE: {}, DEV: {}, CODE: {}".format(state, self.getId(), code))
            return False
        else:
            self.log.debug("SETTED STATE: {}, DEV: {}".format(state, self.getId()))
            return True

    def getTemperatureState(self):
        res = {
            'type': CapTypes.RANGE.value,
            'state': {
                'instance': 'temperature',
                'value': self.getTemperature()
            }
        }
        return res

    def getCapabilities(self):
        cap_dim = {'type': CapTypes.RANGE.value,
                   'retrievable': True,
                   'parameters': {
                       'instance': 'temperature',
                       'unit': 'unit.temperature.celsius',
                       'range': self.range
                        }
                   }
        cap_on_off = {
            'type': CapTypes.ON_OFF.value
        }
        return [cap_dim, cap_on_off]
