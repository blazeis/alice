from libs.ACDevice import ACDevice
from libs.DimmableDevice import DimmableDevice
from libs.OnOffDevice import OnOffDevice
from libs.Device import Device
from libs.Enums import DevTypes


class DeviceManager:
    def __init__(self, config, logger):
        self.__config = config
        self.devices = []
        self.__log = logger

        dev = DimmableDevice(self.__config, self.__log)
        dev.setId("mqtt.0.noolite.module.ch-1")
        dev.setName("свет в спальне")
        dev.setDescription("управление светильником в спальне")
        dev.setRoom("спальня")
        dev.setDevType(DevTypes.LIGHT)
        dev.setManufacturer("NooLite")
        dev.setModel("SUF-1-300")
        self.devices.append(dev)

        dev = DimmableDevice(self.__config, self.__log)
        dev.setId("mqtt.0.noolite.module.ch-2")
        dev.setName("свет в детской")
        dev.setDescription("управление светильником в детской")
        dev.setRoom("детская")
        dev.setDevType(DevTypes.LIGHT)
        dev.setManufacturer("NooLite")
        dev.setModel("SUF-1-300")
        self.devices.append(dev)

        dev = DimmableDevice(self.__config, self.__log)
        dev.setId("mqtt.0.noolite.module.ch-3")
        dev.setName("свет в туалете")
        dev.setDescription("управление светильником в туалете")
        dev.setRoom("туалет")
        dev.setDevType(DevTypes.LIGHT)
        dev.setManufacturer("NooLite")
        dev.setModel("SUF-1-300")
        self.devices.append(dev)

        dev = DimmableDevice(self.__config, self.__log)
        dev.setId("mqtt.0.noolite.module.ch-4")
        dev.setName("свет в ванной")
        dev.setDescription("управление светильником в ванной")
        dev.setRoom("ванная")
        dev.setDevType(DevTypes.LIGHT)
        dev.setManufacturer("NooLite")
        dev.setModel("SUF-1-300")
        self.devices.append(dev)

        dev = DimmableDevice(self.__config, self.__log)
        dev.setId("mqtt.0.noolite.module.ch-5")
        dev.setName("свет в коридоре")
        dev.setDescription("управление светильником в коридоре")
        dev.setRoom("коридор")
        dev.setDevType(DevTypes.LIGHT)
        dev.setManufacturer("NooLite")
        dev.setModel("SUF-1-300")
        self.devices.append(dev)

        dev = OnOffDevice(self.__config, self.__log)
        dev.setId("mqtt.0.noolite.module.ch-6")
        dev.setName("свет в кладовке")
        dev.setDescription("управление светильником в кладовке")
        dev.setRoom("кладовка")
        dev.setDevType(DevTypes.LIGHT)
        dev.setManufacturer("NooLite")
        dev.setModel("SUF-1-300")
        self.devices.append(dev)

        dev = DimmableDevice(self.__config, self.__log)
        dev.setId("mqtt.0.noolite.module.ch-7")
        dev.setName("свет над стойкой")
        dev.setDescription("управление светильником над барной стойкой")
        dev.setRoom("стойка")
        dev.setDevType(DevTypes.LIGHT)
        dev.setManufacturer("NooLite")
        dev.setModel("SUF-1-300")
        self.devices.append(dev)

        dev = DimmableDevice(self.__config, self.__log)
        dev.setId("mqtt.0.noolite.module.ch-8")
        dev.setName("свет над диваном")
        dev.setDescription("управление светильником над диваном")
        dev.setRoom("комната")
        dev.setDevType(DevTypes.LIGHT)
        dev.setManufacturer("NooLite")
        dev.setModel("SUF-1-300")
        self.devices.append(dev)

        dev = DimmableDevice(self.__config, self.__log)
        dev.setId("mqtt.0.noolite.module.ch-9")
        dev.setName("свет над столом")
        dev.setDescription("управление светильником над столом")
        dev.setRoom("комната")
        dev.setDevType(DevTypes.LIGHT)
        dev.setManufacturer("NooLite")
        dev.setModel("SUF-1-300")
        self.devices.append(dev)

        dev = DimmableDevice(self.__config, self.__log)
        dev.setId("mqtt.0.noolite.module.ch-10")
        dev.setName("свет на кухне")
        dev.setDescription("управление светильником на кухне")
        dev.setRoom("кухня")
        dev.setDevType(DevTypes.LIGHT)
        dev.setManufacturer("NooLite")
        dev.setModel("SUF-1-300")
        self.devices.append(dev)

        dev = OnOffDevice(self.__config, self.__log)
        dev.setId("mqtt.0.noolite.module.ch-12")
        dev.setName("свет на балконе")
        dev.setDescription("управление светильником на балконе")
        dev.setRoom("балкон")
        dev.setDevType(DevTypes.LIGHT)
        dev.setManufacturer("NooLite")
        dev.setModel("SUF-1-300")
        self.devices.append(dev)


        """

        dev = Device(self.__config, self.__log)
        dev.setId("3/1/6")
        dev.setName("свет над столом")
        dev.setDescription("управление светильником над столом")
        dev.setRoom("офис")
        dev.setDevType(DevTypes.LIGHT)
        dev.setManufacturer("Logic Machine")
        dev.setModel("Reactor light 4")
        self.devices.append(dev)

        dev = Device(self.__config, self.__log)
        dev.setId("3/1/4")
        dev.setName("свет над экраном")
        dev.setDescription("управление светильником над экраном")
        dev.setRoom("офис")
        dev.setDevType(DevTypes.LIGHT)
        dev.setManufacturer("Logic Machine")
        dev.setModel("Reactor light 4")
        self.devices.append(dev)

        dev = ACDevice(self.__config, self.__log)
        dev.setId("2/2/2")
        dev.setTempId("3/1/1")
        dev.setName("кондиционер")
        dev.setDescription("управление кондиционером")
        dev.setRoom("офис")
        dev.setDevType(DevTypes.THERMOSTAT)
        dev.setManufacturer("Logic Machine")
        dev.setModel("Reactor light 4")
        dev.range['min'] = 18
        dev.range['max'] = 32
        dev.range['precision'] = 1
        self.devices.append(dev)

        dev = ReceiverDevice(self.__config, self.__log)
        dev.setId("2/4/5")
        dev.setChannelId("2/4/4")
        dev.setName("ресивер")
        dev.setDescription("управление громкостью")
        dev.setRoom("офис")
        dev.setDevType(DevTypes.MEDIA_DEVICE)
        dev.setManufacturer("Logic Machine")
        dev.setModel("Reactor light 4")
        dev.range['min'] = 0
        dev.range['max'] = 80
        dev.range['precision'] = 4
        dev.range_channel['min'] = 3
        dev.range_channel['max'] = 6
        dev.range_channel['precision'] = 1
        self.devices.append(dev)

        dev = OnOffDevice(self.__config, self.__log)
        dev.setId("2/4/3")
        dev.setName("презентация")
        dev.setDescription("управление зоной презентации")
        dev.setRoom("офис")
        dev.setDevType(DevTypes.MEDIA_DEVICE)
        dev.setManufacturer("Logic Machine")
        dev.setModel("Reactor light 4")
        self.devices.append(dev)
        """