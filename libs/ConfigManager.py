import configparser
import sys
import os


class ConfigManager:

    def __init__(self, logger):
        self.log = logger
        self.__config = configparser.ConfigParser()
        self.__base_uri = "/"
        self.__api_url = "http://127.0.0.1/"
        self.__oauth_user = "oauth_user"
        self.__oauth_pass = "oauth_pass"
        self.__config_path = os.path.abspath(os.path.dirname(sys.argv[0]))
        self.__config_file = os.path.join(self.__config_path, 'settings.cfg')
        self.__readConfig()
        self.log.info("ConfigManager загружен")

    def __readConfig(self):

        def getOption(name, var):
            return self.__config.getboolean(section, name) if self.__config.has_option(section, name) else var

        def getValue(name, var):
            return self.__config.get(section, name) if self.__config.has_option(section, name) else var

        section = str("web")
        self.__config.read(self.__config_file)
        if self.__config.has_section(section):
            self.log.debug('Конфиг найден: {}. Грузим'.format(self.__config_file))
            self.__base_uri = getValue('base_uri', self.__base_uri)
            self.__api_url = getValue('api_url', self.__api_url)
            self.__oauth_user = getValue('oauth_user', self.__oauth_user)
            self.__oauth_pass = getValue('oauth_pass', self.__oauth_pass)
        else:
            self.log.debug('Конфиг не найден. Создаём')
            self.__writeConfig()
            self.log.debug('Новый конфиг создан')
        self.log.debug("Загруженный конфиг:\n{}".format(self.dumpConfig()))

    def __writeConfig(self):
        section = str("web")
        if self.__config.has_section(section):
            self.__config.remove_section(section)
        self.__config.add_section(section)
        self.__config.set(section, 'base_uri', str(self.__base_uri))
        self.__config.set(section, 'api_url', str(self.__api_url))
        self.__config.set(section, 'oauth_user', str(self.__oauth_user))
        self.__config.set(section, 'oauth_pass', str(self.__oauth_pass))
        self.log.debug('Записываем новый конфиг: {}'.format(self.__config_file))
        with open(self.__config_file, 'w+') as configfile:
            self.__config.write(configfile)

    def dumpConfig(self):
        res = "\n####################################################################\n"
        res += "## base_uri:   {:50s} ##\n".format(self.__base_uri)
        res += "## api_url:    {:50s} ##\n".format(self.__api_url)
        res += "## oauth_user: {:50s} ##\n".format(self.__oauth_user)
        res += "## oauth_pass: {:50s} ##\n".format(self.__oauth_pass)
        res += "####################################################################\n"
        return res

    def getBaseUri(self):
        return self.__base_uri

    def setBaseUri(self, uri: str = "/"):
        self.__base_uri = uri if uri[-1] == "/" else "{}/".format(uri)
        self.__writeConfig()

    def getApiUrl(self):
        return self.__api_url

    def setApiUrl(self, url: str = "http://127.0.0.1/"):
        self.__api_url = url
        self.__writeConfig()

    def getOauthUser(self):
        return self.__oauth_user

    def setOauthUser(self, user: str = "oauth_user"):
        self.__oauth_user = user
        self.__writeConfig()

    def getOauthPass(self):
        return self.__oauth_pass

    def setOauthPass(self, passwd: str = "oauth_pass"):
        self.__oauth_pass = passwd
        self.__writeConfig()
