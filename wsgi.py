#!/usr/bin/python3
# coding=utf-8

from libs.webapp import app  # Импортируем наше приложение

if __name__ == "__main__":
    app.run()  # запускаем его
